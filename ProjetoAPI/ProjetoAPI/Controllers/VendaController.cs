﻿using Microsoft.AspNetCore.Mvc;
using ProjetoAPI.Context;
using ProjetoAPI.Entities;

namespace ProjetoAPI.Controllers
{
    [ApiController]
    [Route("/")]
    public class VendaController : ControllerBase
    {
        private VendaContext _context;

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda()
        {
            _context = new VendaContext();

            Pedido pedido = _context.AdicionarVenda();

            var dados = new
            {
                pedido = pedido,
            };

            return Ok(dados);
        }

        [HttpGet("BuscarVenda/{id}")]
        public IActionResult BuscarVenda(int id)
        {
            _context = new VendaContext();

            Pedido pedido = _context.BuscarVendaPorId(id);

            var dados = new
            {
                pedido = pedido,
            };

            return Ok(dados);
        }

        [HttpPut("AtualizarVenda/{id}")]
        public IActionResult AtualizarVenda(int id)
        {
            _context = new VendaContext();

            Pedido pedido = _context.AtualizarVendaPorId(id);

            var dados = new
            {
                pedido = pedido,
            };

            return Ok(dados);
        }
    }
}
