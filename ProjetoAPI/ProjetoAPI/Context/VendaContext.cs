﻿using ProjetoAPI.Entities;

namespace ProjetoAPI.Context
{
    public class VendaContext
    {
        public Pedido AdicionarVenda()
        {
            Pessoa vendedor1 = new Pessoa(1, "011.022.000-02", "Paulo", "paulo@email.com", "111111");
            Produto produto1 = new Produto(1, "Caderno", 2, 8.50M);
            Produto produto2 = new Produto(2, "Livro", 2, 150.25M);

            StatusVenda valor = StatusVenda.Aguardando;
            Enum status = (StatusVenda)valor;
            string statusVenda = status.ToString();

            if (statusVenda.Equals("Aguardando"))
            {
                statusVenda = "Aguardando Pagamento";
            }

            Pedido pedido1 = new Pedido(1, 10501, vendedor1, DateTime.Now, statusVenda);
            pedido1.Produtos = new List<Produto>();
            pedido1.Produtos.Add(produto1);
            pedido1.Produtos.Add(produto2);

            return pedido1;
        }

        public Pedido BuscarVendaPorId(int id)
        {
            // Pedido 1
            Pessoa vendedor1 = new Pessoa(1, "011.022.000-02", "Paulo", "paulo@email.com", "111111");
            Produto produto1 = new Produto(1, "Caderno", 2, 8.50M);
            Produto produto2 = new Produto(2, "Livro", 2, 150.25M);

            StatusVenda valor = StatusVenda.Aguardando;
            Enum status = (StatusVenda)valor;
            string statusVenda = status.ToString();

            if (statusVenda.Equals("Aguardando"))
            {
                statusVenda = "Aguardando Pagamento";
            }

            Pedido pedido1 = new Pedido(1, 10501, vendedor1, DateTime.Now, statusVenda);
            pedido1.Produtos = new List<Produto>();
            pedido1.Produtos.Add(produto1);
            pedido1.Produtos.Add(produto2);

            if (pedido1.Id == id)
                return pedido1;
            else
                return null;
        }

        public Pedido AtualizarVendaPorId(int id)
        {
            // Pedido 1
            Pessoa vendedor1 = new Pessoa(1, "011.022.000-02", "Paulo", "paulo@email.com", "111111");
            Produto produto1 = new Produto(1, "Caderno", 2, 8.50M);
            Produto produto2 = new Produto(2, "Livro", 2, 150.25M);

            StatusVenda valor1 = StatusVenda.Aprovado;
            Enum status1 = (StatusVenda)valor1;
            string statusVenda1 = status1.ToString();

            if (statusVenda1.Equals("Aprovado"))
            {
                statusVenda1 = "Pagamento Aprovado";
            }

            Pedido pedido1 = new Pedido(1, 10501, vendedor1, DateTime.Now, statusVenda1);
            pedido1.Produtos = new List<Produto>();
            pedido1.Produtos.Add(produto1);
            pedido1.Produtos.Add(produto2);

            if (pedido1.Id == id)
                return pedido1;
            else
                return null;
        }
    }
}
